opam-version: "2.0"
maintainer:   "anil@recoil.org"
authors:      ["Anil Madhavapeddy" "Richard Mortier" "Thomas Gazagnaire"
               "Pierre Chambart" "David Kaloper" "Jeremy Yallop" "David Scott"
               "Mindy Preston" "Thomas Leonard" "Etienne Millon" ]
homepage:     "https://github.com/mirage/ocaml-cstruct"
license:      "ISC"
dev-repo: "git+https://github.com/mirage/ocaml-cstruct.git"
bug-reports:  "https://github.com/mirage/ocaml-cstruct/issues"
doc: "https://mirage.github.io/ocaml-cstruct/"

tags: [ "org:mirage" "org:ocamllabs" ]
build: [
  ["dune" "subst"] {pinned}
  ["dune" "build" "-p" name "-j" jobs]
  ["dune" "runtest" "-p" name "-j" jobs] {with-test & ocaml:version < "4.08.0"}
]
depends: [
  "ocaml" {>= "4.03.0"}
  "dune" {build & >= "1.0"}
  "cstruct" {=version}
  "ounit" {with-test}
  "ppx_tools_versioned" {>= "5.0.1"}
  "ocaml-migrate-parsetree"
  "ppx_sexp_conv" {with-test & < "v0.13"}
  "sexplib" {< "v0.13"}
  "cstruct-sexp" {with-test}
  "cstruct-unix" {with-test & =version}
]
synopsis: "Access C-like structures directly from OCaml"
description: """
Cstruct is a library and syntax extension to make it easier to access C-like
structures directly from OCaml.  It supports both reading and writing to these
structures, and they are accessed via the `Bigarray` module."""
url {
  src:
    "https://github.com/mirage/ocaml-cstruct/releases/download/v4.0.0/cstruct-v4.0.0.tbz"
  checksum: [
    "md5=e01a2036d1fb08d1d8e47d4f3e122bf0"
    "sha256=2a54e291ccf71691fd80b5b80600e2dd5e68d91b8b8f2788f326355305d38ee0"
    "sha512=519fbfa045eeb7d1fc791cda2d88025853e186af73308de9750162a95dafb667711eac280b38c68761d7cfe38801eb35704026f7faebc091b61c04807aaebc4c"
  ]
}
